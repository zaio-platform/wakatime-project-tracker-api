const mongoose = require("mongoose");
const User = require("./user");


const projectSchema = mongoose.Schema({
  project: 
    {
      name: String,
      time_spent: String
    }
  ,
//   user: {
//     type: mongoose.Schema.Types.ObjectId,
//     ref: "User"
//   }
});
const Project = mongoose.model("project", projectSchema);
module.exports = Project;

// module.exports.saveProject = (db, projects, user) => {
//   db.projects.save({
//     projects: projects,
//     user: user
//   });
// };
