const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const userSchema = mongoose.Schema({
name: {
    type: String,
    trim: true,
    required: [true, 'Please Enter Developer Name']
},

apiKey: {
    type: String, 
    unique: true,
    required: [true, 'Please Enter Developer Your API Key'],
    
},

projects:{
    type:Array
}
})

// Hashing the ApiKey before saving it to the database
userSchema.pre('save', async function(next) {
    try {
        const salt = await bcrypt.genSalt(10);
        this.apiKey = await bcrypt.hash(this.apiKey, salt);
        next();
    } catch (error) {
        next(error);
    }
});

//validating the ApiKey
userSchema.methods.validateApiKey = async function(apiKey) {
    return bcrypt.compare(apiKey, this.apiKey);
};

const User = mongoose.model('User', userSchema);
module.exports = User;
