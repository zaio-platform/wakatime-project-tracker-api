const express = require("express");
const app = express();
const db = require("./config/connection");
const UserRoutes = require("./routes/user");
const bodyParser = require("body-parser");
app.set("view engine", "ejs");
app.use(bodyParser.urlencoded({ extended: true }));

app.use("/api", UserRoutes);

app.listen(3000);


console.log("Now Listening To Port 3000");
