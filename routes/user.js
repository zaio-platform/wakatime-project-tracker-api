const express = require ("express");
const router = express.Router();
const fetch = require ("node-fetch");
const bcrypt = require('bcrypt');
const User = require ("../models/user");
//const usersHandler = require("../handlers/user");
const mongoose = require("mongoose");
const db = require("../config/connection");
//const fs = require("fs");

//11fd7625-dec9-4cae-822a-53e7ddc162a2
//router.post("/userdata", usersHandler.post);

router.post("/home", (req, res) => {
  var name = req.body.username;
  var apikey = req.body.apikey;
  let url = "https://wakatime.com/api/v1/users/current/stats/last_30_days";

  let myProjects = [];

  fetch(`${url}?api_key=${apikey}`, { method: "GET" })
    .then(res => res.json())
    .then(async resData => {
      //console.log(resData);
      resData.data.projects.map(project => {
        const pr = {
          "projectName": project.name,
          "timeCoded": project.text
        };
        myProjects.push(pr);
      });

      console.log(myProjects);

      const user = new User({
        name: name,
        apiKey: apikey,
        projects: myProjects
      });
      console.log(user.projects[0])
      try {
        user.save();
      } catch (error) {
        console.log("The User Was Not Save ", error);
      }
      // check if the user's ApiKey is correct
     
     /* const isValid = await user.validateApiKey(req.body.apikey);
        
      if (!isValid) {
          console.log('The Entered ApiKey May Be Incorrect.');
      }
      */
      // res.send({ user });

      //  fs.writeFile("users.pdf", user, err => {
      //   if (err) {
      //     console.log("User Info Not Saved In File", err);
      //     return;
      //   }
      //   console.log("User Saved In File");
      // });
      
      res.render('user', {user: user});
    });
});

router.get("/home", (req, res) => {
  res.render("home");
});

module.exports = router;
